import puzzle_0601 as p1

def intersection(deep_orbit_map, a, b):
    for space_object in deep_orbit_map[a]:
        if space_object in deep_orbit_map[b]:
            return space_object
    return None

def shortest_path(deep_orbit_map, starting_object, destination_object):
    if destination_object in deep_orbit_map[starting_object]:
        return deep_orbit_map[starting_object].index(destination_object)
    closer_starting_object = deep_orbit_map[starting_object][0]
    closer_destination_object = deep_orbit_map[destination_object][0]
    intersection_object = intersection(deep_orbit_map, closer_starting_object, closer_destination_object)
    return deep_orbit_map[closer_starting_object].index(intersection_object) + deep_orbit_map[closer_destination_object].index(intersection_object) + 2


if __name__ == '__main__':
    with open('day06/input') as input_file:
        orbits = [p1.parse_orbit(orbit) for orbit in input_file.read().splitlines()]
        deep_orbit_map = p1.create_deep_orbit_map(orbits)
        print(f"{shortest_path(deep_orbit_map, 'YOU', 'SAN')=}")
