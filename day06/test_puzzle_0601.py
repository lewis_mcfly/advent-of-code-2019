import puzzle_0601 as p

def test_parse_orbit():
    assert p.parse_orbit('COM)B') == ('COM', 'B')
    assert p.parse_orbit('B)C') == ('B', 'C')
    assert p.parse_orbit('6L2)F8N') == ('6L2', 'F8N')

def test_create_orbit_map():
    assert p.create_orbit_map([p.parse_orbit(orbit) for orbit in ['COM)B', 'B)C', 'C)D', 'D)E', 'E)F', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K', 'K)L']]) == {
        'L': 'K',
        'K': 'J',
        'J': 'E',
        'F': 'E',
        'E': 'D',
        'I': 'D',
        'D': 'C',
        'C': 'B',
        'H': 'G',
        'G': 'B',
        'B': 'COM',
    }

def test_create_deep_orbit_map():
    assert p.create_deep_orbit_map([p.parse_orbit(orbit) for orbit in ['COM)B', 'B)C', 'C)D', 'D)E', 'E)F', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K', 'K)L']]) == {
        'L': ['K', 'J', 'E', 'D', 'C', 'B', 'COM'],
        'K': ['J', 'E', 'D', 'C', 'B', 'COM'],
        'J': ['E', 'D', 'C', 'B', 'COM'],
        'F': ['E', 'D', 'C', 'B', 'COM'],
        'E': ['D', 'C', 'B', 'COM'],
        'I': ['D', 'C', 'B', 'COM'],
        'D': ['C', 'B', 'COM'],
        'C': ['B', 'COM'],
        'H': ['G', 'B', 'COM'],
        'G': ['B', 'COM'],
        'B': ['COM'],
    }

def test_orbits_count():
    assert p.orbits_count({
        'L': ['K', 'J', 'E', 'D', 'C', 'B', 'COM'],
        'K': ['J', 'E', 'D', 'C', 'B', 'COM'],
        'J': ['E', 'D', 'C', 'B', 'COM'],
        'F': ['E', 'D', 'C', 'B', 'COM'],
        'E': ['D', 'C', 'B', 'COM'],
        'I': ['D', 'C', 'B', 'COM'],
        'D': ['C', 'B', 'COM'],
        'C': ['B', 'COM'],
        'H': ['G', 'B', 'COM'],
        'G': ['B', 'COM'],
        'B': ['COM'],
    }) == 42
