def parse_orbit(string_orbit):
    parsed = string_orbit.split(")")
    if len(parsed) != 2:
        raise ValueError("Wrong orbit format")
    return parsed[0], parsed[1]

def space_object_parents(orbit_map, space_object):
    if space_object not in orbit_map or len(orbit_map[space_object]) == 0:
        return []
    else:
        parent = orbit_map[space_object]
        return [parent] + space_object_parents(orbit_map, parent)

def create_orbit_map(orbits):
    orbit_map = dict()
    for space_object, in_orbit in orbits:
        orbit_map[in_orbit] = space_object
    return orbit_map

def create_deep_orbit_map(orbits):
    orbit_map = create_orbit_map(orbits)
    deep_orbit_map = dict()
    for space_object in orbit_map:
        deep_orbit_map[space_object] = space_object_parents(orbit_map, space_object)
    return deep_orbit_map

def orbits_count(deep_orbit_map):
    return sum([len(deep_orbit_map[space_object]) for space_object in deep_orbit_map])

if __name__ == '__main__':
    with open('day06/input') as input_file:
        orbits = [parse_orbit(orbit) for orbit in input_file.read().splitlines()]
        deep_orbit_map = create_deep_orbit_map(orbits)
        print(f"{orbits_count(deep_orbit_map)=}")
