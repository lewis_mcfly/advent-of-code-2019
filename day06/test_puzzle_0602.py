import puzzle_0601 as p1
import puzzle_0602 as p2

def test_create_deep_orbit_map():
    assert p1.create_deep_orbit_map([p1.parse_orbit(orbit) for orbit in ['COM)B', 'B)C', 'C)D', 'D)E', 'E)F', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K', 'K)L', 'K)YOU', 'I)SAN']]) == {
        'L': ['K', 'J', 'E', 'D', 'C', 'B', 'COM'],
        'YOU': ['K', 'J', 'E', 'D', 'C', 'B', 'COM'],
        'K': ['J', 'E', 'D', 'C', 'B', 'COM'],
        'J': ['E', 'D', 'C', 'B', 'COM'],
        'F': ['E', 'D', 'C', 'B', 'COM'],
        'E': ['D', 'C', 'B', 'COM'],
        'SAN': ['I', 'D', 'C', 'B', 'COM'],
        'I': ['D', 'C', 'B', 'COM'],
        'D': ['C', 'B', 'COM'],
        'C': ['B', 'COM'],
        'H': ['G', 'B', 'COM'],
        'G': ['B', 'COM'],
        'B': ['COM'],
    }

def test_shortest_path():
    assert p2.shortest_path({
        'L': ['K', 'J', 'E', 'D', 'SAN', 'B', 'COM'],
        'YOU': ['K', 'J', 'E', 'D', 'SAN', 'B', 'COM'],
        'K': ['J', 'E', 'D', 'SAN', 'B', 'COM'],
        'J': ['E', 'D', 'SAN', 'B', 'COM'],
        'F': ['E', 'D', 'SAN', 'B', 'COM'],
        'E': ['D', 'SAN', 'B', 'COM'],
        'I': ['D', 'SAN', 'B', 'COM'],
        'D': ['SAN', 'B', 'COM'],
        'SAN': ['B', 'COM'],
        'H': ['G', 'B', 'COM'],
        'G': ['B', 'COM'],
        'B': ['COM'],
    }, 'YOU', 'SAN') == 4
    assert p2.shortest_path({
        'L': ['K', 'J', 'E', 'D', 'C', 'B', 'COM'],
        'YOU': ['K', 'J', 'E', 'D', 'C', 'B', 'COM'],
        'K': ['J', 'E', 'D', 'C', 'B', 'COM'],
        'J': ['E', 'D', 'C', 'B', 'COM'],
        'F': ['E', 'D', 'C', 'B', 'COM'],
        'E': ['D', 'C', 'B', 'COM'],
        'SAN': ['I', 'D', 'C', 'B', 'COM'],
        'I': ['D', 'C', 'B', 'COM'],
        'D': ['C', 'B', 'COM'],
        'C': ['B', 'COM'],
        'H': ['G', 'B', 'COM'],
        'G': ['B', 'COM'],
        'B': ['COM'],
    }, 'YOU', 'SAN') == 4
