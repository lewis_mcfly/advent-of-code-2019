import cProfile
from puzzle_1202 import  Coordinates, Moon, system_equals, steps_to_origin, compute_lcm

def test_compute_lcm():
    assert compute_lcm([18, 24, 36]) == 72

def test_steps_to_origin():
    assert steps_to_origin({
        'io': Moon(Coordinates(-1, 0, 2)),
        'europa': Moon(Coordinates(2, -10, -7)),
        'ganymede': Moon(Coordinates(4, -8, 8)),
        'callisto': Moon(Coordinates(3, 5, -1))
    }) == 2772
    assert steps_to_origin({
        'io': Moon(Coordinates(-8, -10, 0)),
        'europa': Moon(Coordinates(5, 5, 10)),
        'ganymede': Moon(Coordinates(2, -7, 3)),
        'callisto': Moon(Coordinates(9, -8, -3))
    }) == 4686774924

if __name__ == '__main__':
    cmd = 'steps_to_origin({"io": Moon(Coordinates(-1, 0, 2)), "europa": Moon(Coordinates(2, -10, -7)), "ganymede": Moon(Coordinates(4, -8, 8)), "callisto": Moon(Coordinates(3, 5, -1))}, verbose=True)'
    cProfile.run(cmd, sort='cumulative')
    cmd = 'steps_to_origin({"io": Moon(Coordinates(-8, -10, 0)), "europa": Moon(Coordinates(5, 5, -10)), "ganymede": Moon(Coordinates(2, -7, 3)), "callisto": Moon(Coordinates(9, -8, -3))}, verbose=True)'
    cProfile.run(cmd, sort='cumulative')

