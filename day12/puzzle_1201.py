from dataclasses import dataclass, field
import re

@dataclass
class Coordinates():
    x: int
    y: int
    z: int

    def __add__(self, other):
        x = self.x + other.x
        y = self.y + other.y
        z = self.z + other.z
        return Coordinates(x, y, z)
    
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z
    
    def absolute_values(self):
        return abs(self.x) + abs(self.y) + abs(self.z)
    
    def clone(self):
        return Coordinates(self.x, self.y, self.z)

@dataclass
class Moon():
    coordinates: Coordinates
    velocity: Coordinates = field(default_factory=lambda: Coordinates(0, 0, 0))

    def apply_velocity(self):
        self.coordinates += self.velocity
    
    def potential_energy(self):
        return self.coordinates.absolute_values()
    
    def kinetic_energy(self):
        return self.velocity.absolute_values()
    
    def total_energy(self):
        return self.potential_energy() * self.kinetic_energy()
    
    def clone(self):
        return Moon(self.coordinates.clone(), velocity=self.velocity.clone())

def parse_coordinates(line):
    pattern = re.compile(r"<x=(?P<x>-?\d+), y=(?P<y>-?\d+), z=(?P<z>-?\d+)>")
    match = pattern.match(line).groupdict()
    return Coordinates(int(match['x']), int(match['y']), int(match['z']))

def apply_gravity(moons):
    for moon_a in moons:
        for moon_b in moons:
            if moon_a != moon_b:
                moons[moon_a].velocity.x += -1 if moons[moon_a].coordinates.x > moons[moon_b].coordinates.x else 1 if moons[moon_a].coordinates.x < moons[moon_b].coordinates.x else 0
                moons[moon_a].velocity.y += -1 if moons[moon_a].coordinates.y > moons[moon_b].coordinates.y else 1 if moons[moon_a].coordinates.y < moons[moon_b].coordinates.y else 0
                moons[moon_a].velocity.z += -1 if moons[moon_a].coordinates.z > moons[moon_b].coordinates.z else 1 if moons[moon_a].coordinates.z < moons[moon_b].coordinates.z else 0
    return moons

def apply_velocity(moons):
    for moon in moons:
        moons[moon].apply_velocity()
    return moons

def system_energy(moons):
    energy = 0
    for moon in moons:
        energy += moons[moon].total_energy()
    return energy

if __name__ == '__main__':
    with open('day12/input') as input_file:
        coordinates_array = [parse_coordinates(line) for line in input_file.read().splitlines()]
        moons = dict()
        for index, coordinates in enumerate(coordinates_array):
            moons[index] = Moon(coordinates)
        for _ in range(1000):
            moons = apply_gravity(moons)
            moons = apply_velocity(moons)
        print(f"{system_energy(moons)=}")
