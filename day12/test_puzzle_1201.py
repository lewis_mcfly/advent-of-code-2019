from puzzle_1201 import parse_coordinates, Coordinates, apply_gravity, Moon, apply_velocity, system_energy

def test_parse_coordinates():
    assert parse_coordinates('<x=-1, y=0, z=2>') == Coordinates(-1, 0, 2)
    assert parse_coordinates('<x=2, y=-10, z=-7>') == Coordinates(2, -10, -7)
    assert parse_coordinates('<x=4, y=-8, z=8>') == Coordinates(4, -8, 8)
    assert parse_coordinates('<x=3, y=5, z=-1>') == Coordinates(3, 5, -1)

def test_coordinates_add():
    assert Coordinates(-1, 6, 4) + Coordinates(4, 2, -5) == Coordinates(3, 8, -1)

def test_apply_gravity():
    assert apply_gravity({
        'io': Moon(Coordinates(-1, 0, 2)),
        'europa': Moon(Coordinates(2, -10, -7)),
        'ganymede': Moon(Coordinates(4, -8, 8)),
        'callisto': Moon(Coordinates(3, 5, -1))
    }) == {
        'io': Moon(Coordinates(-1, 0, 2), velocity=Coordinates(3, -1, -1)),
        'europa': Moon(Coordinates(2, -10, -7), velocity=Coordinates(1, 3, 3)),
        'ganymede': Moon(Coordinates(4, -8, 8), velocity=Coordinates(-3, 1, -3)),
        'callisto': Moon(Coordinates(3, 5, -1), velocity=Coordinates(-1, -3, 1))
    }

def test_apply_moon_velocity():
    moon = Moon(Coordinates(-1, 0, 2), velocity=Coordinates(3, -1, -1))
    moon.apply_velocity()
    assert moon == Moon(Coordinates(2, -1, 1), velocity=Coordinates(3, -1, -1))

def test_apply_velocity():
    assert apply_velocity({
        'io': Moon(Coordinates(-1, 0, 2), velocity=Coordinates(3, -1, -1)),
        'europa': Moon(Coordinates(2, -10, -7), velocity=Coordinates(1, 3, 3)),
        'ganymede': Moon(Coordinates(4, -8, 8), velocity=Coordinates(-3, 1, -3)),
        'callisto': Moon(Coordinates(3, 5, -1), velocity=Coordinates(-1, -3, 1))
    }) == {
        'io': Moon(Coordinates(2, -1, 1), velocity=Coordinates(3, -1, -1)),
        'europa': Moon(Coordinates(3, -7, -4), velocity=Coordinates(1, 3, 3)),
        'ganymede': Moon(Coordinates(1, -7, 5), velocity=Coordinates(-3, 1, -3)),
        'callisto': Moon(Coordinates(2, 2, 0), velocity=Coordinates(-1, -3, 1))
    }

def test_coordinates_absolute_values():
    assert Coordinates(-1, 0, 2).absolute_values() == 3
    assert Coordinates(-5, 3, 9).absolute_values() == 17

def test_potential_energy():
    assert Moon(Coordinates(-1, 0, 2)).potential_energy() == 3
    assert Moon(Coordinates(-5, 3, 9)).potential_energy() == 17

def test_kinetic_energy():
    assert Moon(Coordinates(-1, 0, 2), velocity=Coordinates(-3, -2, 1)).kinetic_energy() == 6
    assert Moon(Coordinates(-5, 3, 9), velocity=Coordinates(-1, 1, 3)).kinetic_energy() == 5

def test_total_energy():
    assert Moon(Coordinates(-1, 0, 2), velocity=Coordinates(-3, -2, 1)).total_energy() == 18
    assert Moon(Coordinates(-5, 3, 9), velocity=Coordinates(-1, 1, 3)).total_energy() == 85

def test_system_energy():
    assert system_energy({
        'io': Moon(Coordinates(-1, 0, 2), velocity=Coordinates(3, -1, -1)),
        'europa': Moon(Coordinates(2, -10, -7), velocity=Coordinates(1, 3, 3)),
        'ganymede': Moon(Coordinates(4, -8, 8), velocity=Coordinates(-3, 1, -3)),
        'callisto': Moon(Coordinates(3, 5, -1), velocity=Coordinates(-1, -3, 1))
    }) == 3*5 + 19*7 + 20*7 + 9*5

def test_ten_steps():
    moons = {
        'io': Moon(Coordinates(-1, 0, 2)),
        'europa': Moon(Coordinates(2, -10, -7)),
        'ganymede': Moon(Coordinates(4, -8, 8)),
        'callisto': Moon(Coordinates(3, 5, -1))
    }
    for _ in range(10):
        moons = apply_gravity(moons)
        moons = apply_velocity(moons)
    assert moons == {
        'io': Moon(Coordinates(2, 1, -3), velocity=Coordinates(-3, -2, 1)),
        'europa': Moon(Coordinates(1, -8, 0), velocity=Coordinates(-1, 1, 3)),
        'ganymede': Moon(Coordinates(3, -6, 1), velocity=Coordinates(3, 2, -3)),
        'callisto': Moon(Coordinates(2, 0, 4), velocity=Coordinates(1, -1, -1))
    }
    assert system_energy(moons) == 179

def test_hundred_steps():
    moons = {
        'io': Moon(Coordinates(-8, -10, 0)),
        'europa': Moon(Coordinates(5, 5, 10)),
        'ganymede': Moon(Coordinates(2, -7, 3)),
        'callisto': Moon(Coordinates(9, -8, -3))
    }
    for _ in range(100):
        moons = apply_gravity(moons)
        moons = apply_velocity(moons)
    assert moons == {
        'io': Moon(Coordinates(8, -12, -9), velocity=Coordinates(-7, 3, 0)),
        'europa': Moon(Coordinates(13, 16, -3), velocity=Coordinates(3, -11, -5)),
        'ganymede': Moon(Coordinates(-29, -11, -1), velocity=Coordinates(-3, 7, 4)),
        'callisto': Moon(Coordinates(16, -13, 23), velocity=Coordinates(7, 1, 1))
    }
    assert system_energy(moons) == 1940
