from dataclasses import dataclass, field
import re, math
from enum import IntEnum
from functools import reduce

class Dimension(IntEnum):
    X = 0
    Y = 1
    Z = 2

@dataclass
class Coordinates():
    x: int
    y: int
    z: int
    
    def clone(self):
        return Coordinates(self.x, self.y, self.z)

@dataclass
class Moon():
    coordinates: Coordinates
    velocity: Coordinates = field(default_factory=lambda: Coordinates(0, 0, 0))

    def apply_velocity(self, dimension):
        if dimension is Dimension.X:
            self.coordinates.x += self.velocity.x
        elif dimension is Dimension.Y:
            self.coordinates.y += self.velocity.y
        elif dimension is Dimension.Z:
            self.coordinates.z += self.velocity.z
    
    def potential_energy(self, dimension):
        if dimension is Dimension.X:
            return abs(self.coordinates.x)
        elif dimension is Dimension.Y:
            return abs(self.coordinates.y)
        elif dimension is Dimension.Z:
            return abs(self.coordinates.z)
    
    def kinetic_energy(self, dimension):
        if dimension is Dimension.X:
            return abs(self.velocity.x)
        elif dimension is Dimension.Y:
            return abs(self.velocity.y)
        elif dimension is Dimension.Z:
            return abs(self.velocity.z)
    
    def total_energy(self, dimension):
        return self.potential_energy(dimension) * self.kinetic_energy(dimension)
    
    def clone(self):
        return Moon(self.coordinates.clone(), velocity=self.velocity.clone())

def parse_coordinates(line):
    pattern = re.compile(r"<x=(?P<x>-?\d+), y=(?P<y>-?\d+), z=(?P<z>-?\d+)>")
    match = pattern.match(line).groupdict()
    return Coordinates(int(match['x']), int(match['y']), int(match['z']))

def apply_gravity(moons, dimension):
    for moon_a in moons:
        for moon_b in moons:
            if moon_a != moon_b:
                if dimension is Dimension.X:
                    moons[moon_a].velocity.x += -1 if moons[moon_a].coordinates.x > moons[moon_b].coordinates.x else 1 if moons[moon_a].coordinates.x < moons[moon_b].coordinates.x else 0
                elif dimension is Dimension.Y:
                    moons[moon_a].velocity.y += -1 if moons[moon_a].coordinates.y > moons[moon_b].coordinates.y else 1 if moons[moon_a].coordinates.y < moons[moon_b].coordinates.y else 0
                elif dimension is Dimension.Z:
                    moons[moon_a].velocity.z += -1 if moons[moon_a].coordinates.z > moons[moon_b].coordinates.z else 1 if moons[moon_a].coordinates.z < moons[moon_b].coordinates.z else 0
    return moons

def apply_velocity(moons, dimension):
    for moon in moons:
        moons[moon].apply_velocity(dimension)
    return moons

def system_energy(moons):
    energy = 0
    for moon in moons:
        energy += moons[moon].total_energy()
    return energy

def system_equals(system_1, system_2, dimension):
    if system_1.keys() != system_2.keys():
        return False
    for moon in system_1:
        m1 = system_1[moon]
        m2 = system_2[moon]
        if dimension is Dimension.X and (m1.coordinates.x != m2.coordinates.x or m1.velocity.x != m2.velocity.x):
            return False
        elif dimension is Dimension.Y and (m1.coordinates.y != m2.coordinates.y or m1.velocity.y != m2.velocity.y):
            return False
        elif dimension is Dimension.Z and (m1.coordinates.z != m2.coordinates.z or m1.velocity.z != m2.velocity.z):
            return False
    return True

def system_copy(system):
    copy = dict()
    for moon in system:
        copy[moon] = system[moon].clone()
    return copy

def compute_lcm(numbers, verbose=False):
    lcm = reduce(lambda a, b: abs(a * b) // math.gcd(a, b), numbers)
    return lcm

def steps_to_origin(system, verbose=False):
    periods = list()
    for dimension in Dimension:
        n = 0
        moons = system_copy(system)
        while True:
            moons = apply_gravity(moons, dimension)
            moons = apply_velocity(moons, dimension)
            if system_equals(system, moons, dimension):
                break
            n += 1
            if verbose and n % 25000 == 0:
                print(f"{n=}...", end='', flush=True)
        if verbose:
            print(f"Dimension found {(n+1)=}")
        periods.append(n + 1)
    return compute_lcm(periods, verbose=verbose)

if __name__ == '__main__':
    with open('day12/input') as input_file:
        coordinates_array = [parse_coordinates(line) for line in input_file.read().splitlines()]
        moons = dict()
        for index, coordinates in enumerate(coordinates_array):
            moons[index] = Moon(coordinates)
        n = steps_to_origin(moons, verbose=True)
        print(f"{n=}")
