import puzzle_0401 as p

def has_two_adjacents(value):
    string_value = str(value)
    for i in range(len(string_value) - 1):
        if string_value[i] == string_value[i+1] and (i == 0 or string_value[i-1] != string_value[i]) and (i == len(string_value) - 2 or string_value[i+2] != string_value[i]):
            return True
    return False

def is_valid(value):
    return p.is_in_range(value) and has_two_adjacents(value) and p.is_ascending(value)

if __name__ == '__main__':
    valid_passwords = [n for n in range(p.limits[0], p.limits[1] + 1) if is_valid(n)]
    print(f"{len(valid_passwords)=}")
