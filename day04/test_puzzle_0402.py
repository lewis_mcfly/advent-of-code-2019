import puzzle_0402 as p

def test_has_two_adjacents():
    assert p.has_two_adjacents(111111) == False
    assert p.has_two_adjacents(223450) == True
    assert p.has_two_adjacents(123789) == False
    assert p.has_two_adjacents(555555) == False
    assert p.has_two_adjacents(900000) == False
    assert p.has_two_adjacents(112233) == True
    assert p.has_two_adjacents(123444) == False
    assert p.has_two_adjacents(111122) == True

def test_is_valid():
    assert p.is_valid(111111) == False
    assert p.is_valid(223450) == False
    assert p.is_valid(123789) == False
    assert p.is_valid(555555) == False
    assert p.is_valid(900000) == False
    assert p.is_valid(112233) == False
    assert p.is_valid(123444) == False
    assert p.is_valid(111122) == False
    assert p.is_valid(555566) == True
    assert p.is_valid(567789) == True
