import puzzle_0401 as p

def test_is_in_range():
    assert p.is_in_range(111111) == False
    assert p.is_in_range(223450) == False
    assert p.is_in_range(123789) == False
    assert p.is_in_range(555555) == True
    assert p.is_in_range(900000) == False

def test_has_two_adjacents():
    assert p.has_two_adjacents(111111) == True
    assert p.has_two_adjacents(223450) == True
    assert p.has_two_adjacents(123789) == False
    assert p.has_two_adjacents(555555) == True
    assert p.has_two_adjacents(900000) == True

def test_is_ascending():
    assert p.is_ascending(111111) == True
    assert p.is_ascending(223450) == False
    assert p.is_ascending(123789) == True
    assert p.is_ascending(555555) == True
    assert p.is_ascending(900000) == False

def test_is_valid():
    assert p.is_valid(111111) == False
    assert p.is_valid(223450) == False
    assert p.is_valid(123789) == False
    assert p.is_valid(555555) == True
    assert p.is_valid(900000) == False
