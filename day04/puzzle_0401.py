limits = [359282, 820401]

def is_in_range(value):
    return value >= limits[0] and value <= limits[1]

def has_two_adjacents(value):
    string_value = str(value)
    for i in range(len(string_value) - 1):
        if string_value[i] == string_value[i+1]:
            return True
    return False

def is_ascending(value):
    digits = [int(d) for d in str(value)]
    for i in range(len(digits) - 1):
        if digits[i] > digits[i+1]:
            return False
    return True

def is_valid(value):
    return is_in_range(value) and has_two_adjacents(value) and is_ascending(value)

if __name__ == '__main__':
    valid_passwords = [n for n in range(limits[0], limits[1] + 1) if is_valid(n)]
    print(f"{len(valid_passwords)=}")
