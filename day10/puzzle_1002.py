from pathlib import Path
from math import atan2, hypot, pi
from operator import itemgetter
from collections import defaultdict

def read_map(raw_text):
    asteroids = list()
    for y, row in enumerate(raw_text.split()):
        for x, col in enumerate(list(row)):
            if col == "#":
                asteroids.append((x, y))

    return asteroids

def angle_from(a, b):
    return atan2(b[0] - a[0], a[1] - b[1]) % (2 * pi)

def visible_targets(check, asteroids):
    visible = set()
    for asteroid in asteroids:
        if asteroid == check:
            continue
        angle = angle_from(check, asteroid)
        visible.add(angle)

    return visible

def get_best_location(asteroid_map):
    most_visible = 0
    best_location = None
    for asteroid in asteroid_map:
        visible = visible_targets(asteroid, asteroid_map)
        if most_visible < len(visible):
            most_visible = len(visible)
            best_location = asteroid

    return best_location, most_visible

def dist_from(a, b):
    ax, ay = a
    bx, by = b

    return hypot(by - ay, bx - ax)

def destroy_order(blocks, station):
    return lambda asteroid: (blocks[asteroid], angle_from(station, asteroid))

def two_hundredth_asteroid(asteroids):
    visibles = [(a, len(visible_targets(a, asteroids))) for a in asteroids]
    visibles.sort(key=itemgetter(1))
    station = visibles[-1][0]
    asteroids.remove(station)

    asteroids.sort(key=lambda a: dist_from(station, a))
    blocks = defaultdict(lambda: 0)
    for a, asteroid in enumerate(asteroids):
        closest_asteroids = asteroids[:a]
        for close_asteroid in closest_asteroids:
            angle1 = angle_from(station, asteroid)
            angle2 = angle_from(station, close_asteroid)
            if angle1 == angle2:
                block = blocks[asteroid]
                blocks[asteroid] = block + 1

    order = destroy_order(blocks, station)
    return sorted(asteroids, key=lambda a: order(a))[200 - 1]

if __name__ == "__main__":
    with open('day10/input') as input_file:
        asteroid_map = read_map(input_file.read())
        x, y = two_hundredth_asteroid(asteroid_map)
        print(f"{x=} {y=} {(x * 100 + y)=}")
