from puzzle_1002 import read_map, visible_targets, get_best_location, two_hundredth_asteroid

def test_visible_targets():
    with open(f'day10/test_input_1') as input_file:
        space_map = read_map(input_file.read())
        assert len(visible_targets((1, 2), space_map)) == 7
        assert len(visible_targets((1, 0), space_map)) == 7
        assert len(visible_targets((4, 2), space_map)) == 5
        assert len(visible_targets((0, 2), space_map)) == 6
        assert len(visible_targets((4, 4), space_map)) == 7
        assert len(visible_targets((3, 4), space_map)) == 8
    
    with open(f'day10/test_input_2') as input_file:
        space_map = read_map(input_file.read())
        assert len(visible_targets((5, 8), space_map)) == 33
    
    with open(f'day10/test_input_3') as input_file:
        space_map = read_map(input_file.read())
        assert len(visible_targets((1, 2), space_map)) == 35

    with open(f'day10/test_input_4') as input_file:
        space_map = read_map(input_file.read())
        assert len(visible_targets((6, 3), space_map)) == 41

    with open(f'day10/test_input_5') as input_file:
        space_map = read_map(input_file.read())
        assert len(visible_targets((11, 13), space_map)) == 210

def test_get_best_location():
    with open(f'day10/test_input_1') as input_file:
        position, count = get_best_location(read_map(input_file.read()))
        assert (position, count) == ((3, 4), 8)

    with open(f'day10/test_input_2') as input_file:
        position, count = get_best_location(read_map(input_file.read()))
        assert (position, count) == ((5, 8), 33)
    
    with open(f'day10/test_input_3') as input_file:
        position, count = get_best_location(read_map(input_file.read()))
        assert (position, count) == ((1, 2), 35)

    with open(f'day10/test_input_4') as input_file:
        position, count = get_best_location(read_map(input_file.read()))
        assert (position, count) == ((6, 3), 41)

    with open(f'day10/test_input_5') as input_file:
        position, count = get_best_location(read_map(input_file.read()))
        assert (position, count) == ((11, 13), 210)

def test_two_hundredth_asteroid():
    with open(f'day10/test_input_5') as input_file:
        assert two_hundredth_asteroid(read_map(input_file.read())) == (8, 2)
