from puzzle_1001 import Position, Line, line_of_two_points, print_map, read_map, visible_targets, get_best_position

def test_line_of_two_points():
    assert line_of_two_points(Position(0, 0), Position(1, 0)) == Line(0, 0)
    assert line_of_two_points(Position(0, 0), Position(-1, 0)) == Line(0, 0)
    assert line_of_two_points(Position(0, 0), Position(1, 1)) == Line(1, 0)
    assert line_of_two_points(Position(0, 0), Position(-1, -1)) == Line(1, 0)
    assert line_of_two_points(Position(-2, -1), Position(0, 0)) == Line(0.5, 0)

def test_read_map_print_map():
    for n in range(1, 6):
        with open(f'day10/test_input_{n}') as input_file:
            content = input_file.read()
            assert print_map(read_map(content)) == content
    
def test_visible_targets():
    with open(f'day10/test_input_1') as input_file:
        space_map = read_map(input_file.read())
        assert len(visible_targets(Position(1, 2), space_map)) == 7
        assert len(visible_targets(Position(1, 0), space_map)) == 7
        assert len(visible_targets(Position(4, 2), space_map)) == 5
        assert len(visible_targets(Position(0, 2), space_map)) == 6
        assert len(visible_targets(Position(4, 4), space_map)) == 7
        assert len(visible_targets(Position(3, 4), space_map)) == 8

    with open(f'day10/test_input_2') as input_file:
        space_map = read_map(input_file.read())
        assert len(visible_targets(Position(5, 8), space_map)) == 33
    
    with open(f'day10/test_input_3') as input_file:
        space_map = read_map(input_file.read())
        assert len(visible_targets(Position(1, 2), space_map)) == 35

    with open(f'day10/test_input_4') as input_file:
        space_map = read_map(input_file.read())
        assert len(visible_targets(Position(6, 3), space_map)) == 41

    with open(f'day10/test_input_5') as input_file:
        space_map = read_map(input_file.read())
        assert len(visible_targets(Position(11, 13), space_map)) == 210


def test_get_best_position():
    with open(f'day10/test_input_1') as input_file:
        position, count = get_best_position(read_map(input_file.read()))
        assert (position.x, position.y, count) == (3, 4, 8)

    with open(f'day10/test_input_2') as input_file:
        position, count = get_best_position(read_map(input_file.read()))
        assert (position.x, position.y, count) == (5, 8, 33)
    
    with open(f'day10/test_input_3') as input_file:
        position, count = get_best_position(read_map(input_file.read()))
        assert (position.x, position.y, count) == (1, 2, 35)

    with open(f'day10/test_input_4') as input_file:
        position, count = get_best_position(read_map(input_file.read()))
        assert (position.x, position.y, count) == (6, 3, 41)

    with open(f'day10/test_input_5') as input_file:
        position, count = get_best_position(read_map(input_file.read()))
        assert (position.x, position.y, count) == (11, 13, 210)
