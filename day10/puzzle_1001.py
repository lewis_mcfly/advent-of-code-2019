from dataclasses import dataclass
import decimal
from enum import IntEnum
import math

@dataclass
class Position:
    class Value(IntEnum):
        EMPTY = 0
        ASTEROID = 1
    x: float
    y: float
    value: Value = Value.EMPTY

@dataclass
class Line:
    slope: float
    intercept: float

    def get_x(self, y):
        return (y - self.intercept) / self.slope if self.slope != 0 else 0.0

    def get_y(self, x):
        return (self.slope * x) + self.intercept

def line_of_two_points(a, b):
    slope = (b.y - a.y) / (b.x - a.x)
    intercept = a.y - (slope * a.x)
    return Line(slope, intercept)

def visible_targets(pov, space_map):
    def first_position_to_point(point):
        def is_integer(value):
            is_ceil_close = math.isclose(value, math.ceil(value), abs_tol=0.00001)
            is_floor_close = math.isclose(value, math.floor(value), abs_tol=0.00001)
            return is_ceil_close or is_floor_close
        
        if pov.x == point.x and pov.y == point.y:
            return None
        elif pov.x == point.x:
            increment = 1 if pov.y < point.y else -1
            for y in range(pov.y + increment, point.y + increment, 1 if pov.y < point.y else -1):
                if space_map[y][pov.x].value is Position.Value.ASTEROID:
                    return space_map[y][pov.x]
        elif pov.y == point.y:
            increment = 1 if pov.x < point.x else -1
            for x in range(pov.x + increment, point.x + increment, 1 if pov.x < point.x else -1):
                if space_map[pov.y][x].value is Position.Value.ASTEROID:
                    return space_map[pov.y][x]
        else:
            line = line_of_two_points(pov, point)
            increment = 1 if pov.x < point.x else -1
            for x in range(pov.x + increment, point.x + increment, 1 if pov.x < point.x else -1):
                y = line.get_y(x)
                if is_integer(y) and space_map[round(y)][x].value is Position.Value.ASTEROID:
                    return space_map[round(y)][x]
        return None

    visible = []
    for y, line in enumerate(space_map):
        for x, line in enumerate(line):
            position = first_position_to_point(space_map[y][x])
            if position is not None and position not in visible:
                visible.append(position)
    return visible

def print_map(space_map):
    lines = []
    for line in space_map:
        lines.append(''.join(['.' if position.value == Position.Value.EMPTY else '#' for position in line]))
    return '\n'.join(lines)

def read_map(space_map_content):
    space_map = []
    for y, line in enumerate(space_map_content.splitlines()):
        space_map.append([Position(x, y, Position.Value.ASTEROID if character == '#' else Position.Value.EMPTY) for x, character in enumerate(line)])
    return space_map

def get_best_position(space_map):
    best_position = None
    best_position_count = 0
    for y, line in enumerate(space_map):
        for x, line in enumerate(line):
            position = space_map[y][x]
            if position.value is Position.Value.ASTEROID:
                visible_asteroids = visible_targets(position, space_map)
                if len(visible_asteroids) > best_position_count:
                    best_position, best_position_count = position, len(visible_asteroids)
    return best_position, best_position_count

if __name__ == '__main__':
    with open('day10/input') as input_file:
        space_map = read_map(input_file.read())
        position, count = get_best_position(space_map)
        print(f"{position.x=} {position.y=} {count=}")
