from dataclasses import dataclass
from shapely.geometry import LineString as ShapelySegment
from shapely.geometry import Point as ShapelyPoint

@dataclass
class Point:
    x: int = 0
    y: int = 0

def manhattan(a, b):
    return abs(a.x - b.x) + abs(a.y - b.y)

def parse_path(instructions):
    current_x = 0
    current_y = 0
    points = [Point(current_x, current_y)]
    for direction, distance in [(instruction[0], int(instruction[1:])) for instruction in instructions]:
        if direction == 'U':
            current_y += distance
        elif direction == 'R':
            current_x += distance
        elif direction == 'D':
            current_y -= distance
        elif direction == 'L':
            current_x -= distance
        else:
            raise ValueError('Direction should be "U", "R", "D" or "L"')
        points.append(Point(current_x, current_y))
    return points

def closer_intersection_distance(wire_a, wire_b):
    origin = Point(0, 0)
    intersections = list()
    for i in range(0, len(wire_a) - 1):
        segment_a = ShapelySegment([(wire_a[i].x, wire_a[i].y), (wire_a[i+1].x, wire_a[i+1].y)])
        for j in range(0, len(wire_b) - 1):
            if i == 0 and j == 0:
                break
            segment_b = ShapelySegment([(wire_b[j].x, wire_b[j].y), (wire_b[j+1].x, wire_b[j+1].y)])
            intersection = segment_a.intersection(segment_b)
            intersection = Point(int(intersection.x), int(intersection.y)) if not intersection.is_empty else None
            if intersection is not None:
                intersections.append((intersection, manhattan(origin, intersection)))
    closer_distance = min([distance for point, distance in intersections])
    return closer_distance

if __name__ == '__main__':
    with open('day03/input') as input_file:
        instructions_array = [line.split(",") for line in input_file.read().splitlines()]
        wires = [parse_path(instructions) for instructions in instructions_array]
        closer_distance = closer_intersection_distance(wires[0], wires[1])
        print(f"{closer_distance=}")
