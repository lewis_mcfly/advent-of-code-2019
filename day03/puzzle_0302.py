from dataclasses import dataclass
from shapely.geometry import LineString as ShapelySegment
from shapely.geometry import Point as ShapelyPoint
import puzzle_0301 as p

def wires_intersections(wire_a, wire_b):
    intersections = list()
    for i in range(0, len(wire_a) - 1):
        segment_a = ShapelySegment([(wire_a[i].x, wire_a[i].y), (wire_a[i+1].x, wire_a[i+1].y)])
        for j in range(0, len(wire_b) - 1):
            if i == 0 and j == 0:
                break
            segment_b = ShapelySegment([(wire_b[j].x, wire_b[j].y), (wire_b[j+1].x, wire_b[j+1].y)])
            intersection = segment_a.intersection(segment_b)
            intersection = p.Point(int(intersection.x), int(intersection.y)) if not intersection.is_empty else None
            if intersection is not None:
                intersections.append(intersection)
    return intersections

def steps_to_point(wire, point):
    steps = 0
    for i in range(0, len(wire) - 1):
        a = (wire[i].x, wire[i].y)
        b = (wire[i+1].x, wire[i+1].y)
        segment = ShapelySegment([a, b])
        if segment.intersects(ShapelyPoint(point.x, point.y)):
            segment_to_intersection = ShapelySegment([a, (point.x, point.y)])
            steps += int(segment_to_intersection.length)
            return steps
        else:
            steps += int(segment.length)
    return None

def steps_to_first_intersection(wire_a, wire_b):
    intersections = wires_intersections(wire_a, wire_b)
    steps_to_interactions = [steps_to_point(wire_a, intersection) + steps_to_point(wire_b, intersection) for intersection in intersections]
    minimum_steps = min(steps_to_interactions)
    return minimum_steps

if __name__ == '__main__':
    with open('day03/input') as input_file:
        instructions_array = [line.split(",") for line in input_file.read().splitlines()]
        wires = [p.parse_path(instructions) for instructions in instructions_array]
        print(f"{steps_to_first_intersection(wires[0], wires[1])=}")
