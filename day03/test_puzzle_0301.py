import puzzle_0301 as p

def test_manhattan():
    assert p.manhattan(p.Point(0, 0), p.Point(3, 3)) == 6
    assert p.manhattan(p.Point(3, 3), p.Point(0, 0)) == 6
    assert p.manhattan(p.Point(0, 0), p.Point(3, 0)) == 3
    assert p.manhattan(p.Point(0, 0), p.Point(0, 3)) == 3
    assert p.manhattan(p.Point(0, 0), p.Point(0, 0)) == 0
    assert p.manhattan(p.Point(-1, -1), p.Point(1, 1)) == 4
    assert p.manhattan(p.Point(-1, 0), p.Point(2, 5)) == 8

def test_parse_path():
    assert p.parse_path(['R8', 'U5', 'L5', 'D3']) == [
        p.Point(0, 0),
        p.Point(8, 0),
        p.Point(8, 5),
        p.Point(3, 5),
        p.Point(3, 2)
    ]
    assert p.parse_path(['U7', 'R6', 'D4', 'L4']) == [
        p.Point(0, 0),
        p.Point(0, 7),
        p.Point(6, 7),
        p.Point(6, 3),
        p.Point(2, 3),
    ]

def test_closer_intersection_distance():
    wire_a = p.parse_path('R75,D30,R83,U83,L12,D49,R71,U7,L72'.split(","))
    wire_b = p.parse_path('U62,R66,U55,R34,D71,R55,D58,R83'.split(","))
    assert p.closer_intersection_distance(wire_a, wire_b) == 159
    wire_a = p.parse_path('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51'.split(","))
    wire_b = p.parse_path('U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'.split(","))
    assert p.closer_intersection_distance(wire_a, wire_b) == 135