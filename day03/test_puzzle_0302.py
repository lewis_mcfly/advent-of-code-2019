from shapely.geometry import LineString as ShapelySegment
import puzzle_0301 as p1
import puzzle_0302 as p2

def test_first_intersection_steps():
    wire_a = p1.parse_path('R75,D30,R83,U83,L12,D49,R71,U7,L72'.split(","))
    wire_b = p1.parse_path('U62,R66,U55,R34,D71,R55,D58,R83'.split(","))
    assert p2.steps_to_first_intersection(wire_a, wire_b) == 610
    wire_a = p1.parse_path('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51'.split(","))
    wire_b = p1.parse_path('U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'.split(","))
    assert p2.steps_to_first_intersection(wire_a, wire_b) == 410