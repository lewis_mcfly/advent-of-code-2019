import puzzle_0901 as p

def test_parse_instruction():
    instruction = p.__parse_instruction(p.State([int('1001'), 1, 2, 3, 4, 5, 6]), p.operations)
    assert instruction.operation == p.operations[p.OpCodes.ADD]
    assert instruction.parameter_modes == [p.Modes.POSITION, p.Modes.IMMEDIATE, p.Modes.POSITION]
    assert instruction.parameters == [1, 2, 3]

    instruction = p.__parse_instruction(p.State([int('1002'), 1, 2, 3, 4, 5, 6]), p.operations)
    assert instruction.operation == p.operations[p.OpCodes.MULTIPLY]
    assert instruction.parameter_modes == [p.Modes.POSITION, p.Modes.IMMEDIATE, p.Modes.POSITION]
    assert instruction.parameters == [1, 2, 3]

    instruction = p.__parse_instruction(p.State([int('11002'), 1, 2, 3, 4, 5, 6]), p.operations)
    assert instruction.operation == p.operations[p.OpCodes.MULTIPLY]
    assert instruction.parameter_modes == [p.Modes.POSITION, p.Modes.IMMEDIATE, p.Modes.IMMEDIATE]
    assert instruction.parameters == [1, 2, 3]

    instruction = p.__parse_instruction(p.State([int('00003'), 1, 2, 3, 4, 5, 6]), p.operations)
    assert instruction.operation == p.operations[p.OpCodes.INPUT]
    assert instruction.parameter_modes == [p.Modes.POSITION]
    assert instruction.parameters == [1]

    instruction = p.__parse_instruction(p.State([int('099'), 1, 2, 3, 4, 5, 6]), p.operations)
    assert instruction.operation == p.operations[p.OpCodes.BREAK]
    assert instruction.parameter_modes == []
    assert instruction.parameters == []

    instruction = p.__parse_instruction(p.State([int('004'), 1, 2, 3, 4, 5, 6]), p.operations)
    assert instruction.operation == p.operations[p.OpCodes.OUTPUT]
    assert instruction.parameter_modes == [p.Modes.POSITION]
    assert instruction.parameters == [1]

    instruction = p.__parse_instruction(p.State([int('202'), 1, 2, 3, 4, 5, 6], relative_base=2), p.operations)
    assert instruction.operation == p.operations[p.OpCodes.MULTIPLY]
    assert instruction.parameter_modes == [p.Modes.RELATIVE, p.Modes.POSITION, p.Modes.POSITION]
    assert instruction.parameters == [3, 2, 3]

    instruction = p.__parse_instruction(p.State([int('1001'), 4, 5, 6, 10, 11, 12]), p.operations)
    assert instruction.operation == p.operations[p.OpCodes.ADD]
    assert instruction.parameter_modes == [p.Modes.POSITION, p.Modes.IMMEDIATE, p.Modes.POSITION]
    assert instruction.parameters == [10, 5, 6]

def test_intcode():
    assert p.intcode([3, 0, 4, 0, 99], [666], p.operations, p.__parse_instruction) == ([666, 0, 4, 0, 99], [666])
    assert p.intcode([1101, 100, -1, 4, 0], [], p.operations, p.__parse_instruction) == ([1101, 100, -1, 4, 99], [])
    equals_eight_position_mode = [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(equals_eight_position_mode), [n], p.operations, p.__parse_instruction)
        assert outputs == [1 if n == 8 else 0]

    less_than_eight_position_mode = [3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(less_than_eight_position_mode), [n], p.operations, p.__parse_instruction)
        assert outputs == [1 if n < 8 else 0]

    equals_eight_immediate_mode = [3, 3, 1108, -1, 8, 3, 4, 3, 99]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(equals_eight_immediate_mode), [n], p.operations, p.__parse_instruction)
        assert outputs == [1 if n == 8 else 0]

    less_than_eight_immediate_mode = [3, 3, 1107, -1, 8, 3, 4, 3, 99]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(less_than_eight_immediate_mode), [n], p.operations, p.__parse_instruction)
        assert outputs == [1 if n < 8 else 0]

    equals_zero_position_mode = [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(equals_zero_position_mode), [n], p.operations, p.__parse_instruction)
        assert outputs == [1 if n != 0 else 0]

    equals_zero_immediate_mode = [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(equals_zero_immediate_mode), [n], p.operations, p.__parse_instruction)
        assert outputs == [1 if n != 0 else 0]

    around_eight = [int(n) for n in '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99'.split(",")]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(around_eight), [n], p.operations, p.__parse_instruction)
        assert outputs == [999 if n < 8 else 1000 if n == 8 else 1001]

    clones_itself = [int(n) for n in '109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99'.split(",")]
    _, outputs = p.intcode(list(clones_itself), [], p.operations, p.__parse_instruction)
    assert outputs == clones_itself

    sixteen_digits = [int(n) for n in '1102,34915192,34915192,7,4,7,99,0'.split(",")]
    _, outputs = p.intcode(list(sixteen_digits), [], p.operations, p.__parse_instruction)
    assert len(str(outputs[0])) == 16

    for n in range(1125899906842624, 1125899906842624 + 20):
        output_large_number = [104, n, 99]
        _, outputs = p.intcode(list(output_large_number), [], p.operations, p.__parse_instruction)
        assert outputs == [n]
