import puzzle_0901 as p1

if __name__ == '__main__':
    with open('day09/input') as input_file:
        line = input_file.read().splitlines()[0]
        memory = [int(x) for x in line.split(",")]
        _, outputs = p1.intcode(memory, [2], p1.operations, p1.__parse_instruction)
        print(f"{outputs}")
