from dataclasses import dataclass
import puzzle_0501 as p

ADD = 1
MULTIPLY = 2
INPUT = 3
OUTPUT = 4
JUMP_IF_TRUE = 5
JUMP_IF_FALSE = 6
LESS_THAN = 7
EQUALS = 8
BREAK = 99

def bi_parameter_operation(instructions, pointer, instruction):
    parameter_1 = p.get_parameter_value(instructions, instructions[pointer+1], instruction.parameter_1_mode)
    parameter_2 = p.get_parameter_value(instructions, instructions[pointer+2], instruction.parameter_2_mode)
    if instruction.opcode == JUMP_IF_TRUE:
        return parameter_2 if parameter_1 != 0 else pointer + 3
    elif instruction.opcode == JUMP_IF_FALSE:
        return parameter_2 if parameter_1 == 0 else pointer + 3

def tri_parameter_operation(instructions, pointer, instruction):
    if instruction.parameter_3_mode != 'position':
        raise ValueError('Tri-parameter operations should have position mode in third parameter')
    parameter_1 = p.get_parameter_value(instructions, instructions[pointer+1], instruction.parameter_1_mode)
    parameter_2 = p.get_parameter_value(instructions, instructions[pointer+2], instruction.parameter_2_mode)
    parameter_3 = instructions[pointer+3]
    if instruction.opcode == ADD:
        instructions[parameter_3] = parameter_1 + parameter_2
    elif instruction.opcode == MULTIPLY:
        instructions[parameter_3] = parameter_1 * parameter_2
    elif instruction.opcode == LESS_THAN:
        instructions[parameter_3] = 1 if parameter_1 < parameter_2 else 0
    elif instruction.opcode == EQUALS:
        instructions[parameter_3] = 1 if parameter_1 == parameter_2 else 0
    return pointer + 4

def intcode(instructions, input_value):
    pointer = 0
    outputs = []
    while pointer <= len(instructions) - 1:
        instruction = p.parse_intruction(instructions[pointer])
        if instruction.opcode == ADD or instruction.opcode == MULTIPLY or instruction.opcode == LESS_THAN or instruction.opcode == EQUALS:
            pointer = tri_parameter_operation(instructions, pointer, instruction)
        elif instruction.opcode == JUMP_IF_TRUE or instruction.opcode == JUMP_IF_FALSE:
            pointer = bi_parameter_operation(instructions, pointer, instruction)
        elif instruction.opcode == INPUT or instruction.opcode == OUTPUT:
            if instruction.opcode == INPUT:
                position = instructions[pointer+1]
                instructions[position] = input_value
            elif instruction.opcode == OUTPUT:
                parameter = p.get_parameter_value(instructions, instructions[pointer+1], instruction.parameter_1_mode)
                outputs.append(parameter)
            pointer += 2
        elif instruction.opcode == BREAK:
            break
        else:
            raise ValueError(f"op code {instruction.opcode} does not exist")
    return instructions, outputs

if __name__ == '__main__':
    with open('day05/input') as input_file:
        line = input_file.read().splitlines()[0]
        instructions = [int(x) for x in line.split(",")]
        _, outputs = intcode(instructions, 5)
        diagnostic_code = outputs.pop() if len(outputs) > 0 else None
        print(f"{outputs=} {diagnostic_code=}")
