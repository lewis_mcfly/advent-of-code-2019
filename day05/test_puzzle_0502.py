import puzzle_0502 as p

def test_intcode():
    equals_eight_position_mode = [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(equals_eight_position_mode), n)
        assert outputs == [1 if n == 8 else 0]

    less_than_eight_position_mode = [3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(less_than_eight_position_mode), n)
        assert outputs == [1 if n < 8 else 0]

    equals_eight_immediate_mode = [3, 3, 1108, -1, 8, 3, 4, 3, 99]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(equals_eight_immediate_mode), n)
        assert outputs == [1 if n == 8 else 0]

    less_than_eight_immediate_mode = [3, 3, 1107, -1, 8, 3, 4, 3, 99]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(less_than_eight_immediate_mode), n)
        assert outputs == [1 if n < 8 else 0]

    equals_zero_position_mode = [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(equals_zero_position_mode), n)
        assert outputs == [1 if n != 0 else 0]

    equals_zero_immediate_mode = [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(equals_zero_immediate_mode), n)
        assert outputs == [1 if n != 0 else 0]

    around_eight = [int(n) for n in '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99'.split(",")]
    for n in range(-20, 20):
        _, outputs = p.intcode(list(around_eight), n)
        assert outputs == [999 if n < 8 else 1000 if n == 8 else 1001]
