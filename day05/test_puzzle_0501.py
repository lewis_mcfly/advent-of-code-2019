import puzzle_0501 as p

def test_parse_instruction():
    assert p.parse_intruction(int('1001')) == p.Instruction(1, 'position', 'immediate', 'position')
    assert p.parse_intruction(int('1002')) == p.Instruction(2, 'position', 'immediate', 'position')
    assert p.parse_intruction(int('11002')) == p.Instruction(2, 'position', 'immediate', 'immediate')
    assert p.parse_intruction(int('10002')) == p.Instruction(2, 'position', 'position', 'immediate')
    assert p.parse_intruction(int('103')) == p.Instruction(3, 'immediate', 'position', 'position')
    assert p.parse_intruction(int('00003')) == p.Instruction(3, 'position', 'position', 'position')
    assert p.parse_intruction(int('004')) == p.Instruction(4, 'position', 'position', 'position')
    assert p.parse_intruction(int('004')) == p.Instruction(4, 'position', 'position', 'position')
    assert p.parse_intruction(int('099')) == p.Instruction(99, 'position', 'position', 'position')
    assert p.parse_intruction(int('99')) == p.Instruction(99, 'position', 'position', 'position')

def test_get_parameter_value():
    assert p.get_parameter_value([9, 8, 7, 6, 5], 2, 'immediate') == 2
    assert p.get_parameter_value([9, 8, 7, 6, 5], 0, 'immediate') == 0
    assert p.get_parameter_value([9, 8, 7, 6, 5], 2, 'position') == 7
    assert p.get_parameter_value([9, 8, 7, 6, 5], 0, 'position') == 9

def test_intcode():
    assert p.intcode([3, 0, 4, 0, 99], 666) == ([666, 0, 4, 0, 99], [666])
    assert p.intcode([1101, 100, -1, 4, 0], None) == ([1101, 100, -1, 4, 99], [])
