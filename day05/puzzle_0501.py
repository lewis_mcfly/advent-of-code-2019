from dataclasses import dataclass

@dataclass
class Instruction:
    opcode: int
    parameter_1_mode: str = 'position'
    parameter_2_mode: str = 'position'
    parameter_3_mode: str = 'position'

def parse_intruction(int_instruction):
    string_instruction = str(int_instruction)
    instruction = Instruction(int(string_instruction[-2:]))
    if len(string_instruction) > 2:
        instruction.parameter_1_mode = 'position' if string_instruction[-3] == '0' else 'immediate'
    if len(string_instruction) > 3:
        instruction.parameter_2_mode = 'position' if string_instruction[-4] == '0' else 'immediate'
    if len(string_instruction) > 4:
        instruction.parameter_3_mode = 'position' if string_instruction[-5] == '0' else 'immediate'
    return instruction

def get_parameter_value(instructions, argument, mode):
    return argument if mode == 'immediate' else instructions[argument]

def intcode(instructions, input_value):
    pointer = 0
    outputs = []
    while pointer <= len(instructions) - 1:
        instruction = parse_intruction(instructions[pointer])
        if instruction.opcode == 1 or instruction.opcode == 2:
            if instruction.parameter_3_mode != 'position':
                raise ValueError('Opcode 01 and 02: should have only have position output mode')
            parameter_1 = get_parameter_value(instructions, instructions[pointer+1], instruction.parameter_1_mode)
            parameter_2 = get_parameter_value(instructions, instructions[pointer+2], instruction.parameter_2_mode)
            result_position = instructions[pointer+3]
            instructions[result_position] = parameter_1 + parameter_2 if instruction.opcode == 1 else parameter_1 * parameter_2
            pointer += 4
        elif instruction.opcode == 3:
            position = instructions[pointer+1]
            instructions[position] = input_value
            pointer += 2
        elif instruction.opcode == 4:
            position = instructions[pointer+1]
            outputs.append(instructions[position])
            pointer += 2
        elif instruction.opcode == 99:
            break
        else:
            raise ValueError('opcode should only be 1, 2, 3, 4 or 99')
    return instructions, outputs

if __name__ == '__main__':
    with open('day05/input') as input_file:
        line = input_file.read().splitlines()[0]
        instructions = [int(x) for x in line.split(",")]
        _, outputs = intcode(instructions, 1)
        diagnostic_code = outputs.pop() if len(outputs) > 0 else None
        print(f"{outputs=} {diagnostic_code=}")
