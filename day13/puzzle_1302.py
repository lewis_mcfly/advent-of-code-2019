from dataclasses import dataclass
from enum import IntEnum
from puzzle_1301_intcode import State, OpCodes, operations, __parse_instruction, Operation

@dataclass
class Tile():
    class Type(IntEnum):
        EMPTY = 0
        WALL = 1
        BLOCK = 2
        PADDLE = 3
        BALL = 4

    x: int
    y: int
    type: Type

def draw_tiles(tiles):
    min_x = min([tile[0] for tile in tiles])
    min_y = min([tile[1] for tile in tiles])
    max_x = max([tile[0] for tile in tiles])
    max_y = max([tile[1] for tile in tiles])
    drawing = ''
    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            if (x, y) in tiles:
                tile = tiles[(x, y)]
                if tile.type is Tile.Type.EMPTY:
                    drawing += ' '
                elif tile.type is Tile.Type.WALL:
                    drawing += '#'
                elif tile.type is Tile.Type.BLOCK:
                    drawing += '+'
                elif tile.type is Tile.Type.PADDLE:
                    drawing += '-'
                elif tile.type is Tile.Type.BALL:
                    drawing += 'O'
            else:
                drawing += ' '
        drawing += '\n'
    return drawing

def intcode(memory, inputs, operations, parse_instruction, verbose=False):
    state = State(memory, inputs=inputs)
    tiles = dict()
    score = 0
    paddle_x = None
    ball_x = None
    while state.pointer <= len(memory) - 1:
        instruction = parse_instruction(state, operations)
        if instruction.operation.opcode == OpCodes.BREAK:
            break
        else:
            if instruction.operation.opcode == OpCodes.INPUT:
                next_instruction = 0
                if ball_x is not None and paddle_x is not None:
                    if ball_x < paddle_x:
                        next_instruction = -1
                    elif ball_x > paddle_x:
                        next_instruction = 1
                state.inputs = [next_instruction]
            instruction.execute(state)
        if len(state.outputs) == 3:
            x, y, value = state.outputs[0], state.outputs[1], state.outputs[2]
            if (x, y) == (-1, 0):
                score = value
            else:
                tile_type = Tile.Type(value)
                tiles[(x, y)] = Tile(x, y, tile_type)
                if tile_type is Tile.Type.PADDLE:
                    paddle_x = x
                    if verbose:
                        print(draw_tiles(tiles))
                        print(f"{score=}")
                elif tile_type is Tile.Type.BALL:
                    ball_x = x
                    if verbose:
                        print(draw_tiles(tiles))
                        print(f"{score=}")
            state.outputs = list()
    return score, tiles

def filter_tiles(tile_type, tiles):
    return [tile for tile in tiles.values() if tile.type is tile_type]

if __name__ == '__main__':
    with open('day13/input') as input_file:
        line = input_file.read().splitlines()[0]
        memory = [int(x) for x in line.split(",")]
        memory[0] = 2
        score, tiles = intcode(memory, [0], operations, __parse_instruction, verbose=True)
        print(f"{score=}")