from dataclasses import dataclass
from enum import IntEnum
from puzzle_1301_intcode import State, OpCodes, operations, __parse_instruction, Operation

@dataclass
class Tile():
    class Type(IntEnum):
        EMPTY = 0
        WALL = 1
        BLOCK = 2
        PADDLE = 3
        BALL = 4

    x: int
    y: int
    type: Type

def draw_tiles(tiles):
    min_x = min([tile[0] for tile in tiles])
    min_y = min([tile[1] for tile in tiles])
    max_x = max([tile[0] for tile in tiles])
    max_y = max([tile[1] for tile in tiles])
    drawing = ''
    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            if (x, y) in tiles:
                tile = tiles[(x, y)]
                if tile.type is Tile.Type.EMPTY:
                    drawing += ' '
                elif tile.type is Tile.Type.WALL:
                    drawing += '#'
                elif tile.type is Tile.Type.BLOCK:
                    drawing += '+'
                elif tile.type is Tile.Type.PADDLE:
                    drawing += '-'
                elif tile.type is Tile.Type.BALL:
                    drawing += 'O'
            else:
                drawing += ' '
        drawing += '\n'
    return drawing

def intcode(memory, inputs, operations, parse_instruction):
    state = State(memory)
    state.inputs = iter(list(inputs))
    tiles = dict()
    while state.pointer <= len(memory) - 1:
        instruction = parse_instruction(state, operations)
        if instruction.operation.opcode == OpCodes.BREAK:
            break
        else:
            instruction.execute(state)
        if len(state.outputs) == 3:
            x, y, tile_type = state.outputs[0], state.outputs[1], state.outputs[2]
            tiles[(x, y)] = Tile(x, y, Tile.Type(tile_type))
            state.outputs = list()
    return tiles

if __name__ == '__main__':
    with open('day13/input') as input_file:
        line = input_file.read().splitlines()[0]
        memory = [int(x) for x in line.split(",")]
        tiles = intcode(memory, [1], operations, __parse_instruction)
        print(draw_tiles(tiles))
        block_tiles = [tile for tile in tiles.values() if tile.type is Tile.Type.BLOCK]
        print(f"{len(block_tiles)=}")        
