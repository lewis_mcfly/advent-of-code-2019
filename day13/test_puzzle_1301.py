from puzzle_1301 import intcode, draw_tiles, Tile
from puzzle_1301_intcode import State, OpCodes, operations, __parse_instruction

def test_draw_tiles():
    assert draw_tiles({
        (0, 1): Tile(0, 1, Tile.Type.BLOCK),
        (3, 0): Tile(3, 0, Tile.Type.BALL),
        (1, 1): Tile(1, 1, Tile.Type.PADDLE)
    }) == '   O\n+-  \n'