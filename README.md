[![pipeline status](https://gitlab.com/lewis_mcfly/advent-of-code-2019/badges/master/pipeline.svg)](https://gitlab.com/lewis_mcfly/advent-of-code-2019/commits/master)
[![coverage report](https://gitlab.com/lewis_mcfly/advent-of-code-2019/badges/master/coverage.svg)](https://gitlab.com/lewis_mcfly/advent-of-code-2019/commits/master)

# Advent of Code
## 2019

[https://adventofcode.com/2019](https://adventofcode.com/2019)