def base_step(integers, start_position, operator):
    position_x = integers[start_position + 1]
    x = integers[position_x]
    position_y = integers[start_position + 2]
    y = integers[position_y]
    position_result = integers[start_position + 3]
    result = 0
    if operator == '*':
        result = x * y
    elif operator == '+':
        result = x + y
    integers[position_result] = result
    return integers

def add_step(integers, start_position):
    return base_step(integers, start_position, '+')

def multiply_step(integers, start_position):
    return base_step(integers, start_position, '*')

def intcode(integers):
    position = 0
    while position <= len(integers) - 1:
        if integers[position] == 1:
            integers = add_step(integers, position)
        elif integers[position] == 2:
            integers = multiply_step(integers, position)
        elif integers[position] == 99:
            break
        else:
            raise ValueError('Operation should be 1, 2 or 99')
        position += 4
    return integers

if __name__ == '__main__':
    with open('day02/input') as input_file:
        line = input_file.read().splitlines()[0]
        integers = [int(x) for x in line.split(",")]
        integers[1] = 12
        integers[2] = 2
        result = intcode(integers)
        print(f"First value is {result[0]}")
