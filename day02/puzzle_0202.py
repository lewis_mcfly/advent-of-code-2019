import puzzle_0201

if __name__ == '__main__':
    with open('day02/input') as input_file:
        line = input_file.read().splitlines()[0]
        integers = [int(x) for x in line.split(",")]
        noun_verbs = [(noun, verb) for noun in range(100) for verb in range(100)]
        for noun, verb in noun_verbs:
            result = puzzle_0201.intcode([integers[0], noun, verb] + integers[3:])
            if result[0] == 19690720:
                break
        print(f"{noun=} {verb=} {(100 * noun + verb)=}")
