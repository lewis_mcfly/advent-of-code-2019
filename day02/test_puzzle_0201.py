import puzzle_0201

def test_add_step():
    assert puzzle_0201.add_step([1,9,10,3,2,3,11,0,99,30,40,50], 0) == [1,9,10,70,2,3,11,0,99,30,40,50]

def test_multiply_step():
    assert puzzle_0201.multiply_step([1,9,10,70,2,3,11,0,99,30,40,50], 4) == [3500,9,10,70,2,3,11,0,99,30,40,50]

def test_intcode():
    assert puzzle_0201.intcode([1,0,0,0,99]) == [2,0,0,0,99]
    assert puzzle_0201.intcode([2,3,0,3,99]) == [2,3,0,6,99]
    assert puzzle_0201.intcode([2,4,4,5,99,0]) == [2,4,4,5,99,9801]
    assert puzzle_0201.intcode([1,1,1,4,99,5,6,0,99]) == [30,1,1,4,2,5,6,0,99]
