def get_fuel(mass):
    fuel = int(mass / 3) - 2
    return fuel + get_fuel(fuel) if fuel > 0 else 0

if __name__ == '__main__':
    with open('day01/input') as input_file:
        total_fuel = 0
        for mass in [int(line) for line in input_file.read().splitlines()]:
            fuel = get_fuel(int(mass))
            total_fuel += fuel
            print(f'{mass=}: {fuel=}')
        print(f'{total_fuel=}')
