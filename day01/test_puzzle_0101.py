import puzzle_0101

def test_get_full():
    assert 2 == puzzle_0101.get_fuel(12)
    assert 2 == puzzle_0101.get_fuel(14)
    assert 654 == puzzle_0101.get_fuel(1969)
    assert 33583 == puzzle_0101.get_fuel(100756)
