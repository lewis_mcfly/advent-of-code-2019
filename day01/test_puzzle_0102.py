import puzzle_0102

def test_get_full():
    assert 2 == puzzle_0102.get_fuel(12)
    assert 2 == puzzle_0102.get_fuel(14)
    assert 966 == puzzle_0102.get_fuel(1969)
    assert 50346 == puzzle_0102.get_fuel(100756)
