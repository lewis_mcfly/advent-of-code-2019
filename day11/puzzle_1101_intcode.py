from dataclasses import dataclass, field
from enum import IntEnum
from itertools import permutations
import types

class OpCodes(IntEnum):
    ADD = 1
    MULTIPLY = 2
    INPUT = 3
    OUTPUT = 4
    JUMP_IF_TRUE = 5
    JUMP_IF_FALSE = 6
    LESS_THAN = 7
    EQUALS = 8
    RELATIVE_BASE_OFFSET = 9
    BREAK = 99

class Modes(IntEnum):
    POSITION = 0
    IMMEDIATE = 1
    RELATIVE = 2
    WRITE_POSITION = 3
    WRITE_RELATIVE = 4

@dataclass
class Operation:
    opcode: int
    function: types.FunctionType
    args: int

@dataclass
class Instruction:
    operation: Operation
    parameter_modes: list = field(default_factory=lambda: [])
    parameters: list = field(default_factory=lambda: [])

    def execute(self, state):
        return self.operation.function(state, self.parameters)

@dataclass
class State:
    memory: list = field(default_factory=lambda: [])
    pointer: int = 0
    relative_base: int = 0
    inputs: list = field(default_factory=lambda: [])
    outputs: list = field(default_factory=lambda: [])

    def read(self, position):
        if position < 0:
            raise ValueError('Cannot read a negative position')
        elif position >= len(self.memory):
            self.memory.extend([0] * (position - len(self.memory) + 1))
        return self.memory[position]
    
    def write(self, position, value):
        if position < 0:
            raise ValueError('Cannot write a negative position')
        elif position >= len(self.memory):
            self.memory.extend([0] * (position - len(self.memory) + 1))
        self.memory[position] = value

def __parse_instruction(state, operations):
    def __get_parameter(position, mode, write = False):
        def __check_position_in_memory(position):
            if position >= len(state.memory):
                state.memory.extend([0] * (position - len(state.memory) + 1))
        __check_position_in_memory(position)
        value_position = None
        if mode is Modes.IMMEDIATE:
            value_position = position
        elif mode is Modes.POSITION:
            value_position = state.read(position)
        elif mode is Modes.RELATIVE:
            value_position = state.relative_base + state.read(position)
        __check_position_in_memory(value_position)
        return value_position if write else state.read(value_position)

    string_instruction = str(state.read(state.pointer))
    opcode = int(string_instruction[-2:])
    if opcode not in [code for code in OpCodes]:
        raise ValueError(f"op code {opcode} does not exist")
    instruction = Instruction(operations[opcode])

    for n in range(instruction.operation.args):
        mode = Modes.POSITION
        if len(string_instruction) > n+2:
            mode = Modes(int(string_instruction[-(n+3)]))
        write_mode = (n == 0 and instruction.operation.opcode is OpCodes.INPUT) or n == 2
        parameter = __get_parameter(state.pointer + n + 1, mode, write_mode)
        instruction.parameter_modes.append(mode)
        instruction.parameters.append(parameter)

    return instruction

def __add_operation(state, parameters):
    [parameter_1, parameter_2, parameter_3] = parameters
    state.write(parameter_3, parameter_1 + parameter_2)
    state.pointer += 4

def __multiply_operation(state, parameters):
    [parameter_1, parameter_2, parameter_3] = parameters
    state.write(parameter_3, parameter_1 * parameter_2)
    state.pointer += 4

def __less_than_operation(state, parameters):
    [parameter_1, parameter_2, parameter_3] = parameters
    state.write(parameter_3, 1 if parameter_1 < parameter_2 else 0)
    state.pointer += 4

def __equals_operation(state, parameters):
    [parameter_1, parameter_2, parameter_3] = parameters
    state.write(parameter_3, 1 if parameter_1 == parameter_2 else 0)
    state.pointer += 4

def __jump_if_true_operation(state, parameters):
    [parameter_1, parameter_2] = parameters
    state.pointer = parameter_2 if parameter_1 != 0 else state.pointer + 3

def __jump_if_false_operation(state, parameters):
    [parameter_1, parameter_2] = parameters
    state.pointer = parameter_2 if parameter_1 == 0 else state.pointer + 3

def __input_operation(state, parameters):
    [parameter_1] = parameters
    input_value = next(state.inputs)
    state.write(parameter_1, input_value)
    state.pointer += 2

def __output_operation(state, parameters):
    [parameter_1] = parameters
    state.outputs.append(parameter_1)
    state.pointer += 2

def __relative_base_offset_operation(state, parameters):
    [parameter_1] = parameters
    state.relative_base += parameter_1
    state.pointer += 2

def intcode(memory, inputs, operations, parse_instruction):
    state = State(memory)
    state.inputs = iter(list(inputs))
    counter = 0
    while state.pointer <= len(memory) - 1:
        instruction = parse_instruction(state, operations)
        if instruction.operation.opcode == OpCodes.BREAK:
            break
        else:
            instruction.execute(state)
        counter += 1
    return state.memory, state.outputs

operations = {
    OpCodes.ADD: Operation(OpCodes.ADD, __add_operation, 3),
    OpCodes.MULTIPLY: Operation(OpCodes.MULTIPLY, __multiply_operation, 3),
    OpCodes.INPUT: Operation(OpCodes.INPUT, __input_operation, 1),
    OpCodes.OUTPUT: Operation(OpCodes.OUTPUT, __output_operation, 1),
    OpCodes.JUMP_IF_TRUE: Operation(OpCodes.JUMP_IF_TRUE, __jump_if_true_operation, 2),
    OpCodes.JUMP_IF_FALSE: Operation(OpCodes.JUMP_IF_FALSE, __jump_if_false_operation, 2),
    OpCodes.LESS_THAN: Operation(OpCodes.LESS_THAN, __less_than_operation, 3),
    OpCodes.EQUALS: Operation(OpCodes.EQUALS, __equals_operation, 3),
    OpCodes.RELATIVE_BASE_OFFSET: Operation(OpCodes.RELATIVE_BASE_OFFSET, __relative_base_offset_operation, 1),
    OpCodes.BREAK: Operation(OpCodes.BREAK, None, 0),
}

if __name__ == '__main__':
    with open('day09/input') as input_file:
        line = input_file.read().splitlines()[0]
        memory = [int(x) for x in line.split(",")]
        _, outputs = intcode(memory, [1], operations, __parse_instruction)
        print(f"{outputs}")
