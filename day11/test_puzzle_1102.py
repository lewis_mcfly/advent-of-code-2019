from puzzle_1102 import print_panels, Color

def test_print_panels():
    assert print_panels({(0, 1): Color.BLACK, (1, 0): Color.WHITE, (2, 0): Color.BLACK}) == ' ##\n## \n'
