from puzzle_1101_intcode import State, OpCodes, operations as p9_operations, __parse_instruction, Operation
from dataclasses import dataclass, field
from enum import IntEnum

class Direction(IntEnum):
    LEFT = 0
    RIGHT = 1
    UP = 2
    DOWN = 3

class Color(IntEnum):
    BLACK = 0
    WHITE = 1

@dataclass
class Robot():
    x: int = 0
    y: int = 0
    direction: Direction = Direction.UP
    path: list = field(default_factory=lambda: [(0, 0)])

    def position(self):
        return (self.x, self.y)

    def move(self):
        if self.direction is Direction.UP:
            self.y += 1
        elif self.direction is Direction.RIGHT:
            self.x += 1
        elif self.direction is Direction.DOWN:
            self.y -= 1
        elif self.direction is Direction.LEFT:
            self.x -= 1
        else:
            raise ValueError(f"Direction should be {Direction.LEFT=}, {Direction.RIGHT=}, {Direction.UP=} or {Direction.DOWN=}")
        self.path.append((self.x, self.y))
    
    def turn(self, direction):
        if direction is Direction.LEFT:
            if self.direction is Direction.UP:
                self.direction = Direction.LEFT
            elif self.direction is Direction.RIGHT:
                self.direction = Direction.UP
            elif self.direction is Direction.DOWN:
                self.direction = Direction.RIGHT
            elif self.direction is Direction.LEFT:
                self.direction = Direction.DOWN
        elif direction is Direction.RIGHT:
            if self.direction is Direction.UP:
                self.direction = Direction.RIGHT
            elif self.direction is Direction.RIGHT:
                self.direction = Direction.DOWN
            elif self.direction is Direction.DOWN:
                self.direction = Direction.LEFT
            elif self.direction is Direction.LEFT:
                self.direction = Direction.UP
        else:
            raise ValueError(f"Direction should be {Direction.LEFT=} or {Direction.RIGHT=}")

def __input_operation(state, parameters):
    [parameter_1] = parameters
    input_value = state.inputs[0]
    state.inputs = state.inputs[1:]
    state.write(parameter_1, input_value)
    state.pointer += 2

operations = { **p9_operations, OpCodes.INPUT: Operation(OpCodes.INPUT, __input_operation, 1) }

def intcode(memory, operations, parse_instruction):
    robot = Robot()
    panels = {(0, 0): Color.BLACK}
    state = State(memory)
    state.inputs = [0]
    counter = 0
    next_color = None
    next_direction = None
    while state.pointer <= len(memory) - 1:
        instruction = parse_instruction(state, operations)
        if instruction.operation.opcode == OpCodes.BREAK:
            break
        else:
            instruction.execute(state)
            if instruction.operation.opcode == OpCodes.OUTPUT:
                if next_color is None:
                    next_color = Color(state.outputs.pop())
                elif next_direction is None:
                    next_direction = Direction(state.outputs.pop())
                else:
                    raise ValueError('Next color and direction should have been handled')
        if next_color is not None and next_direction is not None:
            panels[robot.position()] = next_color
            robot.turn(next_direction)
            next_color, next_direction = None, None
            robot.move()
            if robot.position() not in panels:
                panels[robot.position()] = Color.BLACK
            state.inputs = [0 if panels[robot.position()] is Color.BLACK else 1]
        counter += 1
    return panels

if __name__ == '__main__':
    with open('day11/input') as input_file:
        line = input_file.read().splitlines()[0]
        memory = [int(x) for x in line.split(",")]
        panels = intcode(memory, operations, __parse_instruction)
        print(f"{len(panels)}")
