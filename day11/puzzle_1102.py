from puzzle_1101 import operations, __parse_instruction, Robot, State, Color, Direction, OpCodes

def intcode(memory, operations, parse_instruction):
    robot = Robot()
    panels = {(0, 0): Color.BLACK}
    state = State(memory)
    state.inputs = [1]
    counter = 0
    next_color = None
    next_direction = None
    while state.pointer <= len(memory) - 1:
        instruction = parse_instruction(state, operations)
        if instruction.operation.opcode == OpCodes.BREAK:
            break
        else:
            instruction.execute(state)
            if instruction.operation.opcode == OpCodes.OUTPUT:
                if next_color is None:
                    next_color = Color(state.outputs.pop())
                elif next_direction is None:
                    next_direction = Direction(state.outputs.pop())
                else:
                    raise ValueError('Next color and direction should have been handled')
        if next_color is not None and next_direction is not None:
            panels[robot.position()] = next_color
            robot.turn(next_direction)
            next_color, next_direction = None, None
            robot.move()
            if robot.position() not in panels:
                panels[robot.position()] = Color.BLACK
            state.inputs = [0 if panels[robot.position()] is Color.BLACK else 1]
        counter += 1
    return panels

def print_panels(panels):
    min_x = min([x for x, y in panels])
    min_y = min([y for x, y in panels])
    max_x = max([x for x, y in panels])
    max_y = max([y for x, y in panels])
    printed = ''
    for y in range(max_y, min_y - 1, -1):
        for x in range(min_x, max_x + 1):
            panel = (x, y)
            if panel in panels:
                printed += '#' if panels[panel] is Color.WHITE else ' '
            else:
                printed += '#'
        printed += '\n'
    return printed


if __name__ == '__main__':
    with open('day11/input') as input_file:
        line = input_file.read().splitlines()[0]
        memory = [int(x) for x in line.split(",")]
        panels = intcode(memory, operations, __parse_instruction)
        print(f"{len(panels)}")
        print(print_panels(panels))
