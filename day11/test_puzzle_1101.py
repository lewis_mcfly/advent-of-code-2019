import pytest
from puzzle_1101 import Robot, Direction

def test_robot_move():
    robot = Robot()
    assert robot.position() == (0, 0)
    for _ in range(3):
        robot.move()
    assert robot.position() == (0, 3)
    assert robot.path == [(0, 0), (0, 1), (0, 2), (0, 3)]
    robot.direction = Direction.RIGHT
    for _ in range(3):
        robot.move()
    assert robot.path == [(0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (2, 3), (3, 3)]

def test_robot_turn():
    robot = Robot()
    assert robot.direction == Direction.UP
    with pytest.raises(ValueError) as _:
        robot.turn(Direction.UP)
    with pytest.raises(ValueError) as _:
        robot.turn(Direction.DOWN)
    for _ in range(3):
        robot.turn(Direction.LEFT)
    assert robot.direction == Direction.RIGHT
    robot.turn(Direction.LEFT)
    assert robot.direction == Direction.UP
