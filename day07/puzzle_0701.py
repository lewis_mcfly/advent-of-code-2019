from itertools import permutations
from dataclasses import dataclass

ADD = 1
MULTIPLY = 2
INPUT = 3
OUTPUT = 4
JUMP_IF_TRUE = 5
JUMP_IF_FALSE = 6
LESS_THAN = 7
EQUALS = 8
BREAK = 99

@dataclass
class Instruction:
    opcode: int
    parameter_1_mode: str = 'position'
    parameter_2_mode: str = 'position'
    parameter_3_mode: str = 'position'

def parse_instruction(int_instruction):
    string_instruction = str(int_instruction)
    instruction = Instruction(int(string_instruction[-2:]))
    if len(string_instruction) > 2:
        instruction.parameter_1_mode = 'position' if string_instruction[-3] == '0' else 'immediate'
    if len(string_instruction) > 3:
        instruction.parameter_2_mode = 'position' if string_instruction[-4] == '0' else 'immediate'
    if len(string_instruction) > 4:
        instruction.parameter_3_mode = 'position' if string_instruction[-5] == '0' else 'immediate'
    return instruction

def intcode(instructions, input_values):
    def get_parameter_value(argument, mode):
        return argument if mode == 'immediate' else instructions[argument]

    pointer = 0
    outputs = []
    while pointer <= len(instructions) - 1:
        instruction = parse_instruction(instructions[pointer])

        if instruction.opcode in [ADD, MULTIPLY, LESS_THAN, EQUALS]:
            if instruction.parameter_3_mode != 'position':
                raise ValueError('Tri-parameter operations should have position mode in third parameter')
            parameter_1 = get_parameter_value(instructions[pointer+1], instruction.parameter_1_mode)
            parameter_2 = get_parameter_value(instructions[pointer+2], instruction.parameter_2_mode)
            parameter_3 = instructions[pointer+3]
            if instruction.opcode == ADD:
                instructions[parameter_3] = parameter_1 + parameter_2
            elif instruction.opcode == MULTIPLY:
                instructions[parameter_3] = parameter_1 * parameter_2
            elif instruction.opcode == LESS_THAN:
                instructions[parameter_3] = 1 if parameter_1 < parameter_2 else 0
            elif instruction.opcode == EQUALS:
                instructions[parameter_3] = 1 if parameter_1 == parameter_2 else 0
            pointer += 4

        elif instruction.opcode in [JUMP_IF_TRUE, JUMP_IF_FALSE]:
            parameter_1 = get_parameter_value(instructions[pointer+1], instruction.parameter_1_mode)
            parameter_2 = get_parameter_value(instructions[pointer+2], instruction.parameter_2_mode)
            if instruction.opcode == JUMP_IF_TRUE:
                pointer = parameter_2 if parameter_1 != 0 else pointer + 3
            elif instruction.opcode == JUMP_IF_FALSE:
                pointer = parameter_2 if parameter_1 == 0 else pointer + 3

        elif instruction.opcode == INPUT:
            position = instructions[pointer+1]
            if len(input_values) == 0:
                raise ValueError('Input array is empty')
            instructions[position] = input_values[0]
            input_values = input_values[1:]
            pointer += 2

        elif instruction.opcode == OUTPUT:
            parameter = get_parameter_value(instructions[pointer+1], instruction.parameter_1_mode)
            outputs.append(parameter)
            pointer += 2

        elif instruction.opcode == BREAK:
            break

        else:
            raise ValueError(f"op code {instruction.opcode} does not exist")
    return instructions, outputs

def get_output(phase, current_signal, instructions):
    _, outputs = intcode(instructions, [phase, current_signal])
    if len(outputs) != 1:
        raise ValueError('There should be only one output')
    return outputs[0]

def thruster_signal(instructions, sequence):
    signal = 0
    for phase in sequence:
        signal = get_output(phase, signal, list(instructions))
    return signal

def max_thruster_signal(instructions):
    possible_sequences = [list(permutation) for permutation in permutations(range(5), 5)]
    maximum = max([thruster_signal(instructions, sequence) for sequence in possible_sequences])
    return maximum

if __name__ == '__main__':
    with open('day07/input') as input_file:
        instructions = [int(x) for x in input_file.read().splitlines()[0].split(",")]
        print(f"{max_thruster_signal(instructions)=}")