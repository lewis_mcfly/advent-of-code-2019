from itertools import permutations
from dataclasses import dataclass, field

import puzzle_0701 as p

@dataclass
class Amp():
    instructions: list
    phase: int
    pointer: int = 0
    stopped: bool = False
    initialized: bool = False
    output = None

    def intcode(self, inputs):
        def get_parameter_value(argument, mode):
            return argument if mode == 'immediate' else self.instructions[argument]

        if self.stopped:
            return
        while self.pointer <= len(self.instructions) - 1:
            instruction = p.parse_instruction(self.instructions[self.pointer])
            if instruction.opcode in [p.ADD, p.MULTIPLY, p.LESS_THAN, p.EQUALS]:
                if instruction.parameter_3_mode != 'position':
                    raise ValueError('Tri-parameter operations should have position mode in third parameter')
                parameter_1 = get_parameter_value(self.instructions[self.pointer+1], instruction.parameter_1_mode)
                parameter_2 = get_parameter_value(self.instructions[self.pointer+2], instruction.parameter_2_mode)
                parameter_3 = self.instructions[self.pointer+3]
                if instruction.opcode == p.ADD:
                    self.instructions[parameter_3] = parameter_1 + parameter_2
                elif instruction.opcode == p.MULTIPLY:
                    self.instructions[parameter_3] = parameter_1 * parameter_2
                elif instruction.opcode == p.LESS_THAN:
                    self.instructions[parameter_3] = 1 if parameter_1 < parameter_2 else 0
                elif instruction.opcode == p.EQUALS:
                    self.instructions[parameter_3] = 1 if parameter_1 == parameter_2 else 0
                self.pointer += 4
            
            elif instruction.opcode in [p.JUMP_IF_TRUE, p.JUMP_IF_FALSE]:
                parameter_1 = get_parameter_value(self.instructions[self.pointer+1], instruction.parameter_1_mode)
                parameter_2 = get_parameter_value(self.instructions[self.pointer+2], instruction.parameter_2_mode)
                if instruction.opcode == p.JUMP_IF_TRUE:
                    self.pointer = parameter_2 if parameter_1 != 0 else self.pointer + 3
                elif instruction.opcode == p.JUMP_IF_FALSE:
                    self.pointer = parameter_2 if parameter_1 == 0 else self.pointer + 3

            elif instruction.opcode == p.INPUT:
                position = self.instructions[self.pointer+1]
                try:
                    self.instructions[position] = next(inputs)
                except:
                    raise ValueError('Inputs is empty')
                self.pointer += 2
            
            elif instruction.opcode == p.OUTPUT:
                parameter = get_parameter_value(self.instructions[self.pointer+1], instruction.parameter_1_mode)
                self.output = parameter
                self.pointer += 2
                return
            
            elif instruction.opcode == p.BREAK:
                self.stopped = True
                return
            
            else:
                raise ValueError(f"op code {instruction.opcode} does not exist")
        raise ValueError(f"No value was output, or no break was found")

def thruster_signal(instructions, sequence):
    amps = [Amp(list(instructions), phase) for phase in sequence]
    signal = 0
    amp_index = 0
    while not amps[amp_index].stopped:
        amp = amps[amp_index]
        if not amp.initialized:
            amp.intcode([amp.phase, signal].__iter__())
            amp.initialized = True
        else:
            amp.intcode([signal].__iter__())
        signal = amp.output
        amp_index = (amp_index+1) % 5
    return amps[4].output

def max_thruster_signal(instructions):
    possible_sequences = [list(permutation) for permutation in permutations(range(5, 10), 5)]
    maximum = max([thruster_signal(instructions, sequence) for sequence in possible_sequences])
    return maximum

if __name__ == '__main__':
    with open('day07/input') as input_file:
        instructions = [int(x) for x in input_file.read().splitlines()[0].split(",")]
        print(f"{max_thruster_signal(instructions)=}")