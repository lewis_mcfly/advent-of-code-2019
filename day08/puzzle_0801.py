def extract_layer(width, height, pixels):
    return pixels[:width * height], pixels[width * height:]

def extract_layers(width, height, pixels):
    layers = []
    while len(pixels) > 0:
        layer, pixels = extract_layer(width, height, pixels)
        layers.append(layer)
    return layers

def fewest_zero_layer(layers):
    return min(layers, key=lambda l: l.count('0'))

WIDTH = 25
HEIGHT = 6
if __name__ == '__main__':
    with open('day08/input') as input_file:
        pixels = input_file.read().splitlines()[0]
        layers = extract_layers(WIDTH, HEIGHT, pixels)
        layer = fewest_zero_layer(layers)
        one_counts = layer.count('1')
        two_counts = layer.count('2')
        print(f"{one_counts * two_counts}")