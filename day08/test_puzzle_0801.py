import puzzle_0801 as p

def test_extract_layer():
    assert p.extract_layer(3, 2, '123456789012') == ('123456', '789012')
    assert p.extract_layer(3, 2, '789012') == ('789012', '')

def test_extract_layers():
    assert p.extract_layers(3, 2, '123456789012') == ['123456', '789012']

def test_fewest_zero_layer():
    assert p.fewest_zero_layer(['012345', '056789', '000004', '123456', '001234', '345600']) == '123456'
