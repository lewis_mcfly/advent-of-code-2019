import puzzle_0802 as p

def test_compute_pixel():
    assert p.compute_pixel('0', '0') == '0'
    assert p.compute_pixel('0', '1') == '0'
    assert p.compute_pixel('0', '2') == '0'
    assert p.compute_pixel('1', '0') == '1'
    assert p.compute_pixel('1', '1') == '1'
    assert p.compute_pixel('1', '2') == '1'
    assert p.compute_pixel('2', '0') == '0'
    assert p.compute_pixel('2', '1') == '1'
    assert p.compute_pixel('2', '2') == '2'

def test_compute_pixels():
    assert p.compute_pixels(['0', '1', '2', '0']) == '0'
    assert p.compute_pixels(['2', '1', '2', '0']) == '1'
    assert p.compute_pixels(['2', '2', '1', '0']) == '1'
    assert p.compute_pixels(['2', '2', '2', '0']) == '0'

def test_decode_image():
    assert p.decode_image([['0', '2', '2', '2'], ['1', '1', '2', '2'], ['2', '2', '1', '2'], ['0', '0', '0', '0']]) == ['0', '1', '1', '0']
