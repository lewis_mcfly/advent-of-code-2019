from functools import reduce
import textwrap
import puzzle_0801 as p1

BLACK = '0'
WHITE = '1'
TRANSPARENT = '2'

def compute_pixel(top, bottom):
    return bottom if top is TRANSPARENT else top

def compute_pixels(pixels):
    return reduce(compute_pixel, pixels)

def decode_image(layers):
    return [compute_pixels(pixels) for pixels in list(zip(*layers))]

if __name__ == '__main__':
    with open('day08/input') as input_file:
        pixels = input_file.read().splitlines()[0]
        layers = p1.extract_layers(p1.WIDTH, p1.HEIGHT, pixels)
        image = decode_image(layers)
        readable_image = ''.join(['#' if character is WHITE else ' ' for character in image])
        lines = textwrap.wrap(readable_image, p1.WIDTH)
        print('\n'.join(lines))
